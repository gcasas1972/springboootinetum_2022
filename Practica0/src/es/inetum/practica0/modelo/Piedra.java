package es.inetum.practica0.modelo;

public class Piedra extends PiedraPapelTijeraFactory {
	public Piedra() {
		this("Piedra", PiedraPapelTijeraFactory.PIEDRA);
	}
	
	public Piedra(String nombre, int numero) {
		super(nombre, numero);		
	}

	@Override
	public boolean isMe(int pPiePapelTijera) {		
		return pPiePapelTijera == PiedraPapelTijeraFactory.PIEDRA;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedPapelTijFac) {
		// TODO Auto-generated method stub
		return 0;
	}

}
