package es.inetum.practica0.modelo.test;


import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import es.inetum.practica0.modelo.Papel;
import es.inetum.practica0.modelo.Piedra;
import es.inetum.practica0.modelo.PiedraPapelTijeraFactory;
import es.inetum.practica0.modelo.Tijera;

class PiedraPapelTijeraFactoryTest {
	Piedra piedra;
	Papel  papel;
	Tijera tijera;

	@BeforeEach
	void setUp() throws Exception {
		piedra = new Piedra();
		papel = new Papel();
		tijera = new Tijera();
	}

	@AfterEach
	void tearDown() throws Exception {
		piedra 	= null;
		papel 	= null ;
		tijera  = null;
		
	}

	@Test
	void testGetInstancePiedra() {
		
	assertEquals("piedra",	PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.PIEDRA)
					.getNombre()
					.toLowerCase())
	;
	}

	@Test
	void testCompararTijeraConPapel() {
		assertEquals(1, tijera.comparar(papel));
	}

}
