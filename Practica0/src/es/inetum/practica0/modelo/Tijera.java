package es.inetum.practica0.modelo;

public class Tijera extends PiedraPapelTijeraFactory {
	public Tijera() {
		this("tijera",TIJERA);
	}
	
	public Tijera(String pNom, int pNum) {
		super(pNom, pNum);
	}

	@Override
	public boolean isMe(int pNum) {
		return pNum==TIJERA;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedPapelTijera) {
		int resul=0;
		switch (pPiedPapelTijera.getNumero()) {
		case PAPEL:		
			resul=1;
			this.descripcionResultado = "tijera le gana a " + pPiedPapelTijera.getNombre();
			break;
			
        case PIEDRA:
        	resul=-1;
			this.descripcionResultado = "tijera perdi� con " + pPiedPapelTijera.getNombre();
			break;

		default:
			resul=0;
			this.descripcionResultado = "tijera empata con " + pPiedPapelTijera.getNombre();
			break;
		}
		return resul;
	}
}
