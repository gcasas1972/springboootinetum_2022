package es.inetum.practica0.modelo;

public class Papel extends PiedraPapelTijeraFactory {

	public Papel() {
		this("Papel", PiedraPapelTijeraFactory.PAPEL);
	}
	public Papel(String nombre, int numero) {
		super(nombre, numero);		
	}

	@Override
	public boolean isMe(int pPiePapelTijera) {		
		//este metodo queda muy facil...
		return pPiePapelTijera==PiedraPapelTijeraFactory.PAPEL;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedPapelTijFac) {
		// TODO Auto-generated method stub
		return 0;
	}

}
